var webpack = require('webpack');
var path = require("path");
// var ExtractTextPlugin = require("extract-text-webpack-plugin");

var debug = true;

module.exports = {
    entry:{
        'demo.index': ['webpack/hot/dev-server','./demo/dev/index.js'],
        'demo.login': ['webpack/hot/dev-server','./demo/dev/login.js']
    },
    output: {
        path: path.join(__dirname, 'bundle'),
        publicPath: debug ? '../bundle/' : 'resource/',
        filename: '[name].js',
        chunkFilename: '[id].js',
    },
    resolve:{
        extensions:['', '.js', '.jsx', 'css', 'vue']
    },
    module:{
        loaders:[
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.js$/,
                exclude: /node_modules|vue\/src|vue-router\/|vue-loader\/|vue-hot-reload-api\//,
                loader: 'babel-loader'
            },{
                test: /\.css$/,
                //loader: ExtractTextPlugin.extract("style-loader", "css-loader")
                loader: 'style-loader!css-loader'
            },{
                test: /\.scss$/,
                //loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")
                loader: 'style-loader!css-loader!sass-loader'

            },{
                test: /\.(png|jpg|gif|JPG)$/,
                loader: 'url-loader?limit=8192'
            },{
                test: /\.woff/,
                loader: 'url-loader?limit=10000&minetype=application/font-woff'
            },{
                test: /\.ttf/,
                loader: 'file-loader'
            },{
                test: /\.eot/,
                loader: 'file-loader'
            },{
                test: /\.svg/,
                loader: 'file-loader'
            },{
                test:/\.html$/,
                loader:'html-loader'
            }
        ]
    },
    babel: {
        presets: ['es2015', 'stage-2'],
        plugins: ['transform-runtime']
    },
    plugins:[
        new webpack.HotModuleReplacementPlugin()
    ]
};